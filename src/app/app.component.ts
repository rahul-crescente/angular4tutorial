import { Component } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { DataService } from './data.service';


@Component({
   selector: 'app-root',
   templateUrl: './app.component.html',
   styleUrls: ['./app.component.css']
})

export class AppComponent {

	posts: Post[];

   constructor(private http: Http, private dataService: DataService) { }

   ngOnInit() {
      this.dataService.getPosts().subscribe((posts) => {
         // console.log(posts);
         this.posts = posts;
      });
   }

   getResponse () {

      this.dataService.getResp().subscribe((res) => {
         console.log(res);
      });

      return false;
   }
}

interface Post {
	id: number,
	title: string,
	body: string
}