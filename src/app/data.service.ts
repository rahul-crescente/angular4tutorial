import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, RequestMethod } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {

 	constructor( public http:Http ) {
  		console.log('data service connected');
  	}

  	getPosts() {
		return this.http.get('https://jsonplaceholder.typicode.com/posts').map(res => res.json());
	}

	getResp () {
    let url = "http://guru.stagingweb.xyz/api_v1/auth/checkemail";

    let headers = new Headers({ 'Content-Type': 'application/json' });

    headers.append('Accept', 'application/json');

    let options = new RequestOptions({ headers: headers });

    let useremail = { "userdata":{ "email_login": "test@gmail.com" } };

    console.log(url);

		return this.http.post(url, JSON.stringify(), options).map(resp => resp.json());
	}

}
